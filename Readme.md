# Customizations

A place to keep my scripts, settings, and other customizations for workstation setup.

## Karabiner Elements
https://karabiner-elements.pqrs.org/ 

Or install with homebrew:
    
    brew install --cask karabiner-elements


### Double-Tap Caps Lock
If you map Caps Lock as an additional CTRL key, you lose Caps Lock functionality. With Karabiner you can add back this functionality. (Found this solution here: https://github.com/pqrs-org/Karabiner-Elements/issues/346)

Copy the double-tap-caps.json to the following directory and then add it as a complex modification in your Karabiner preferences.
 
    ~/.config/karabiner/assets/complex_modifications/

## Toggle File Extension

A macOS quick action to toggle file extensions on/off in Finder. To install, double click the Toggle File Extension workflow, or copy it to:

    ~/Library/Services/

# Open in Visual Studio Code

A macOS quick action to open a file or directory in VS Code. To install, double click the Open in Visual Studio workflow, or copy it to:

    ~/Library/Services/
