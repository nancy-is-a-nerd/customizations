# Brew Install

## Install Homebrew
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

## Setup
	brew install --cask iterm2
	brew install starship
	brew install --cask karabiner-elements

## Development
	brew install rbenv
	brew install --cask visual-studio-code
	brew install --cask beekeeper-studio
	brew install --cask balenaetcher

## Favourite Apps
	brew install --cask telegram
	brew install --cask slack

## Other Useful Apps
	brew install --cask intellij-idea-ce

## Annoying But Necessary
	brew install --cask google-chrome
